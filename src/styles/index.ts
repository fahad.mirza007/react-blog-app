import {
  AppBar,
  Box,
  Button,
  inputBaseClasses,
  outlinedInputClasses,
  styled,
  TextField,
} from "@mui/material";
import SearchOutlinedIcon from "@mui/icons-material/SearchOutlined";
import { Link, NavLink } from "react-router-dom";
import bannerImage from "../images/main-banner.jpeg";
import destinationBannerImage from "../images/destination-banner.jpg";

export const CustomNavbar = styled(AppBar)(() => ({
  backgroundColor: "#FFF",
  borderBottom: "1px solid #D8D8D8",
  padding: "8px 40px",
  position: "sticky",
  top: 0,
}));

export const CustomLink = styled(NavLink)(() => ({
  textDecoration: "none",
  color: "inherit",
}));

export const Wrapper = styled(Box)(() => ({
  maxWidth: 715,
  margin: "93px auto 0",
}));

export const FormContainer = styled("form")(({ theme }) => ({
  marginTop: 72,

  [theme.breakpoints.down("sm")]: {
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
  },
}));

export const ForgotPasswordLink = styled(Link)(() => ({
  float: "right",
  color: "#111111",
}));

export const InputButtonWithBackground = styled(Button)(() => ({
  backgroundColor: "#595858",
  border: "1px solid #595858",
  opacity: 0.25,

  "&:hover": {
    backgroundColor: "#595858",
  },
}));

export const InputButtonWithoutBackground = styled(Button)(() => ({
  backgroundColor: "transparent",
  border: "1px solid #111111",
  color: "#111111",

  "&:hover": {
    borderColor: "#111111",
    backgroundColor: "transparent",
  },
}));

export const NavbarListItemText = styled(Link)(() => ({
  color: "#595858",
  fontSize: 16,
  fontWeight: 400,
  lineHeight: "normal",
  textTransform: "capitalize",
  textDecoration: "none",
  transition: "all .5s ease-in-out",

  "&:hover": {
    color: "#111111",
  },
}));

export const CreateArticleButton = styled(Button)(() => ({
  fontSize: 16,
  fontWeight: 400,
  lineHeight: "normal",
  boxShadow: "unset",
  border: "1px solid #111111",
  padding: "8px 20px",
  borderRadius: 8,
  textTransform: "capitalize",
  marginRight: 40,
  cursor: "pointer",

  "& a": {
    color: "inherit",
    textDecoration: "none",
  },
}));

export const SearchBarInput = styled(TextField)(() => ({
  fontSize: 12,
  fontWeight: 40,
  padding: "8px 16px 8px 40px",
  backgroundColor: "#D8D8D8",
  border: "1px solid #EAEAEA",
  color: "#595858",
  borderRadius: 16,
  lineHeight: "normal",

  [`& .${inputBaseClasses.root}`]: {
    padding: 0,
    fontSize: "unset",
  },

  [`& .${outlinedInputClasses.notchedOutline}`]: {
    border: "unset",
    outline: "none",
  },
}));

export const SearchIcon = styled(SearchOutlinedIcon)(() => ({
  width: 16,
  height: 16,
  position: "absolute",
  top: 10,
  left: 10,
  color: "#595858",
  zIndex: 1,
}));

export const Banner = styled(Box)(() => ({
  background: `url(${bannerImage}) no-repeat center / cover`,
  height: 450,
  padding: 40,
  marginTop: 16,
  borderRadius: 12,

  display: "flex",
  flexDirection: "column",
  justifyContent: "flex-end",
  color: "#FFFFFF",
}));

export const MoreArticleButton = styled(Button)(() => ({
  borderRadius: 32,
  padding: "10px 35px",
  fontSize: 18,
  fontWeight: 500,
}));

export const DestinationWrapper = styled(Box)(() => ({
  position: "absolute",
  inset: 0,
  borderRadius: 12,
  background: "rgba(0, 0, 0, 0.25)",
  padding: 40,

  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  color: "#FFFFFF",
}));

export const DestinationBanner = styled(Box)(() => ({
  background: `url(${destinationBannerImage}) no-repeat center / cover`,
  height: 600,
  borderRadius: 12,
  marginTop: 80,

  position: "relative",
}));

export const DestinationBannerWrapper = styled(Box)(() => ({
  position: "absolute",
  inset: 0,
  backgroundColor: "rgba(52, 58, 64, .5)",
  borderRadius: 12,
  padding: "20px 116px",

  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  color: "#FFFFFF",
}));

export const DetailPageBanner = styled(Box)(() => ({
  background: `url(${destinationBannerImage}) no-repeat center / cover`,
  height: 600,
  borderRadius: 6,
}));

export const CreateArticleFormContainer = styled("form")(({ theme }) => ({
  maxWidth: 715,
  marginTop: 64,
  marginBottom: 64,

  [theme.breakpoints.down("sm")]: {
    marginLeft: 10,
    marginRight: 10,
  },
}));

export const UploadedImage = styled("img")(() => ({
  marginRight: 24,
  width: 104,
  height: 104,
  borderRadius: 12,
}));

export const InputFileWrapper = styled(Box)(() => ({
  "& input": {
    display: "none",
  },

  "& label": {
    color: "#111111",
    padding: 10,
    backgroundColor: "transparent",
    border: "1px solid #111111",
    borderRadius: 48,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: 100,
    fontSize: 12,
    fontWeight: 500,
    lineHeight: "normal",
    cursor: "pointer",
  },
}));
