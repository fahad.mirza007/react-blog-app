export const storyImageStyle = {
  maxWidth: "100%",
  height: "auto",
  borderRadius: "8px",
  marginBottom: "16px",
}

export const destinationImageStyle = {
  display: "block",
  maxWidth: "100%",
  minHeight: "100%",
  borderRadius: "12px"
}

export const destinationColumnStyle = {
  flex: 1,
  position: "relative", 
}

export const searchResultStyle = {
  color: "rgba(102, 102, 102, 0.80)", 
  borderBottom: "1px solid rgba(102, 102, 102, 0.25)",
  paddingTop: "12px",
  paddingBottom: "12px"
}

