import { Box } from "@mui/material"
import { Image } from "./shared/Image"
import { CreateArticleButton, SearchBarInput, SearchIcon } from "../styles"
import { Link } from "react-router-dom"
import { DropDownIcon } from "./DropDownIcon"

export const RightNav = () => {
  return (
    <Box sx={{ display: "flex", alignItems: "center" }}>
      <Box sx={{ mr: 5, position: "relative" }}>
        <SearchIcon />
        <SearchBarInput variant="outlined" placeholder="Search" />
      </Box>

      <CreateArticleButton variant="contained">
        <Link to="/create-article">
          create article
        </Link>
      </CreateArticleButton>

      <Box sx={{ display: "flex", height: 40 }}>
        <Image />
        <DropDownIcon />
      </Box>
    </Box>
  )
}