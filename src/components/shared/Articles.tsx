import { Box, Grid, Stack, Typography } from "@mui/material";
import trendingStoryImage from "../../images/trending-stories.jpeg";
import { BadgeText } from "./BadgeText";
import { ImageUserWrapper } from "./ImageUserWrapper";

const trendingStories = [
  {
    id: 1,
    image: trendingStoryImage,
    title: "The Impact of Technology on the Workplace: How Technology is Changing"
  },
  {
    id: 2,
    image: trendingStoryImage,
    title: "The Impact of Technology on the Workplace: How Technology is Changing"
  },
  {
    id: 3,
    image: trendingStoryImage,
    title: "The Impact of Technology on the Workplace: How Technology is Changing"
  },
  {
    id: 4,
    image: trendingStoryImage,
    title: "The Impact of Technology on the Workplace: How Technology is Changing"
  }
];

export const Articles = () => {
  return (
    <Stack spacing={3}>
      {trendingStories.map((trending) => (
        <Box key={trending.id} sx={{ p: 2, border: "1px solid #E8E8EA", borderRadius: 1.5 }}>
          <Grid container>
            <Grid item sm={5} xs={12}>
              <Box component="img" src={trending.image} alt={trending.image} sx={{ maxWidth: "100%", minHeight: "100%", borderRadius: 1.75 }} />
            </Grid>

            <Grid item sm marginLeft={{ xs: 'unset', sm: 2 }}>
              <Box sx={{ p: 1 }}>
                <BadgeText />
                <Typography variant="h4" sx={{ mt: 2, mb: 3 }}>{trending.title}</Typography>
                <ImageUserWrapper />
              </Box>
            </Grid>
          </Grid>
        </Box>
      ))}
    </Stack>
  );
}