import { Box, Typography } from "@mui/material"
import { Image } from "./Image"

export const ImageUserWrapper = () => {
  return (
    <Box sx={{display: "flex", alignItems: "center"}}>
      <Image />
      <Typography variant="body1" sx={{ textTransform: "capitalize", fontWeight: 500, ml: 1.5 }}>tracey wilson</Typography>
      <Typography component="time" sx={{ ml: 2.5 }}>August 20, 2022</Typography>
    </Box>
  )
}