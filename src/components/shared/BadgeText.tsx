import { Chip } from "@mui/material";

export const BadgeText = () => <Chip component="span" variant="filled" label="technology" sx={{ height: "unset", py: .5 }} />