import { Grid } from "@mui/material";
import { LeftNav } from "../LeftNav";
import { RightNav } from "../RightNav";
import { CustomNavbar } from "../../styles";

export const Navbar = () => {
  return (
    <CustomNavbar>
      <Grid container justifyContent="space-between" alignItems="center">
        <LeftNav />
        <RightNav />
      </Grid>
    </CustomNavbar>
  )
}