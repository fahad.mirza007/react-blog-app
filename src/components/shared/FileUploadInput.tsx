import { InputFileWrapper } from "../../styles"

export const FileUploadInput = () => {
  return (
    <InputFileWrapper>
      <input type="file" id="file" name="file" />
      <label htmlFor="file">Browse</label>
    </InputFileWrapper>
  )
}