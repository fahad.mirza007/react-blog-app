import { Box } from '@mui/material';
import { Link } from 'react-router-dom';
import profileImage from '../../images/profile-image.jpg';

export const Image = () => {
  return (
    <Link to="/">
      <Box component="img" src={profileImage} alt={profileImage} sx={{ width: 40, height: 40, borderRadius: "50%" }} />
    </Link>
  )
}