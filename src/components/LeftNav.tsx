import { Box, List, ListItem } from "@mui/material"
import { Image } from "./shared/Image"
import { NavbarListItemText } from "../styles"

const pages = [
  {
    id: 1,
    path: "/",
    value: "home"
  },
  {
    id: 2,
    path: "/article",
    value: "my article"
  }
];

export const LeftNav = () => {
  return (
    <Box sx={{ display: "flex" }}>
      <Box sx={{ height: 40 }}>
        <Image />
      </Box>

      <List sx={{ display: "flex", alignItems: "center", p: 0 }}>
        {pages.map((page) => (
          <ListItem key={page.id} sx={{ width: "unset", ml: 5, p: 0 }}>
            <NavbarListItemText to={page.path}>{page.value}</NavbarListItemText>
          </ListItem>
        ))
        }
      </List>
    </Box>
  )
}