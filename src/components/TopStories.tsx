import { Box, Grid, Typography } from "@mui/material";
import { Banner, MoreArticleButton } from "../styles";
import { BadgeText } from "./shared/BadgeText";
import { ImageUserWrapper } from "./shared/ImageUserWrapper";
import storyImage from '../images/top-stories.jpeg';
import { storyImageStyle } from "../styles/styleConstants";
import { Link } from "react-router-dom";

const stories = [
  {
    id: 1,
    image: storyImage,
    tag: 'technology',
    title: 'The Impact of Technology on the Workplace: How Technology is Changing',
    userText: 'tracey wilson',
    time: 'August 20, 2022'
  },
  {
    id: 2,
    image: storyImage,
    tag: 'technology',
    title: 'The Impact of Technology on the Workplace: How Technology is Changing',
    userText: 'john wick',
    time: 'August 20, 2022'
  },
  {
    id: 3,
    image: storyImage,
    tag: 'technology',
    title: 'The Impact of Technology on the Workplace: How Technology is Changing',
    userText: 'jason wilson',
    time: 'August 20, 2022'
  },
  {
    id: 4,
    image: storyImage,
    tag: 'technology',
    title: 'The Impact of Technology on the Workplace: How Technology is Changing',
    userText: 'tracey wilson',
    time: 'August 20, 2022'
  },
  {
    id: 5,
    image: storyImage,
    tag: 'technology',
    title: 'The Impact of Technology on the Workplace: How Technology is Changing',
    userText: 'john wick',
    time: 'August 20, 2022'
  },
  {
    id: 6,
    image: storyImage,
    tag: 'technology',
    title: 'The Impact of Technology on the Workplace: How Technology is Changing',
    userText: 'jason wilson',
    time: 'August 20, 2022'
  }
];

export const TopStories = () => {
  return (
    <>
      <Typography variant="h2" sx={{ mx: 1.5, py: 1.5, borderBottom: "1px solid #EAEAEA" }}>top stories</Typography>

      <Banner>
        <BadgeText />
        <Typography variant="h2" sx={{ maxWidth: 782, mt: 2, mb: 3 }}>
          <Link to={'/detail-page'}>
            The Impact of Technology on the Workplace: How Technology is Changing
          </Link>
        </Typography>
        <ImageUserWrapper />
      </Banner>

      <Grid container sx={{ mt: 2 }} spacing={2}>
        {stories.map((story) => (
          <Grid key={story.id} item sm={4} xs={12} >
            <Box sx={{ p: 2, border: "1px solid #E8E8EA", borderRadius: 1.5 }}>
              <Box component="img" src={story.image} alt={story.image} sx={storyImageStyle} />

              <Box sx={{ p: 1 }}>
                <BadgeText />
                <Typography variant="h4" sx={{ mt: 2, mb: 3 }}>{story.title}</Typography>
                <ImageUserWrapper />
              </Box>
            </Box>
          </Grid>
        ))}
      </Grid>

      <Box sx={{ textAlign: "center", mt: 1.5 }}>
        <Link to="/article">
          <MoreArticleButton variant="outlined">view more article</MoreArticleButton>
        </Link>
      </Box>
    </>
  );
}