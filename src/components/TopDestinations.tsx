import { Box, Stack, Typography } from "@mui/material";
import destinationOne from "../images/destination-1.jpg";
import destinationTwo from "../images/destination-2.jpg";
import destinationThree from "../images/destination-3.jpg";
import destinationFour from "../images/destination-4.jpg";
import { destinationColumnStyle, destinationImageStyle } from "../styles/styleConstants";
import { DestinationBanner, DestinationBannerWrapper, DestinationWrapper } from "../styles";
import { BadgeText } from "./shared/BadgeText";

const destinations = [
  {
    id: 1,
    image: destinationOne,
    title: "dominican republic"
  },
  {
    id: 2,
    image: destinationTwo,
    title: "grand canyon"
  },
  {
    id: 3,
    image: destinationThree,
    title: "dubai"
  },
  {
    id: 4,
    image: destinationFour,
    title: "italy"
  }
]

export const TopDestinations = () => {
  return (
    <>
      <Typography variant="h2" sx={{ mx: 1.5, py: 1.5, borderBottom: "1px solid #EAEAEA" }}>top destinations
        <Typography variant="body1" color="#666666" marginTop={2}>
          Tick one more destination off of your bucket list with one of our most popular
          vacations in 2022
        </Typography>
      </Typography>

      <Stack direction="row" spacing={2} sx={{ mt: 4 }}>
        {destinations.map((destination) => (
            <Box key={destination.id} sx={destinationColumnStyle} >
              <Box component="img" src={destination.image} alt={destination.image} sx={destinationImageStyle} />

              <DestinationWrapper>
                <Typography variant="h3" align="center">{destination.title}</Typography>
              </DestinationWrapper>
            </Box>
          ))
        }
      </Stack>

      <DestinationBanner>
        <DestinationBannerWrapper>
          <BadgeText />
          <Typography variant="h1" sx={{ my: 1, textAlign: "center" }}>Richard Norton photorealistic rendering as real photos</Typography>
          <Typography variant="body1">
            Progressively incentivize cooperative systems through technically sound functionalities. The credibly
            productive seamless data.
          </Typography>
        </DestinationBannerWrapper>
      </DestinationBanner>
    </>
  );
} 