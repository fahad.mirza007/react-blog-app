import { Box, Grid, Typography } from "@mui/material";
import { BadgeText } from "./shared/BadgeText";
import { ImageUserWrapper } from "./shared/ImageUserWrapper";
import trendingStoryImage from "../images/trending-stories.jpeg";

const trendingStories = [
  {
    id: 1,
    image: trendingStoryImage,
    title: "The Impact of Technology on the Workplace: How Technology is Changing"
  },
  {
    id: 2,
    image: trendingStoryImage,
    title: "The Impact of Technology on the Workplace: How Technology is Changing"
  },
  {
    id: 3,
    image: trendingStoryImage,
    title: "The Impact of Technology on the Workplace: How Technology is Changing"
  },
  {
    id: 4,
    image: trendingStoryImage,
    title: "The Impact of Technology on the Workplace: How Technology is Changing"
  }

];

export const TrendingStories = () => {
  return (
    <>
      <Typography variant="h2" sx={{ mx: 1.5, py: 1.5, borderBottom: "1px solid #EAEAEA" }}>trending</Typography>

      <Grid container sx={{ mt: 2 }} spacing={2}>
        {trendingStories.map((trending) => (
            <Grid key={trending.id} item sm={6} xs={12} >
              <Box sx={{ p: 2, border: "1px solid #E8E8EA", borderRadius: 1.5 }}>
                <Grid container>
                  <Grid item sm={3} xs={12}>
                    <Box component="img" src={trending.image} alt={trending.image} sx={{ maxWidth: "100%", minHeight: "100%", borderRadius: 2 }} />
                  </Grid>

                  <Grid item sm marginLeft={{ xs: 'unset', sm: 2 }}>
                    <Box sx={{ p: 1 }}>
                      <BadgeText />
                      <Typography variant="h4" sx={{ mt: 2, mb: 3 }}>{trending.title}</Typography>
                      <ImageUserWrapper />
                    </Box>
                  </Grid>
                </Grid>
              </Box>
            </Grid>
          ))
        }
      </Grid>
    </>
  );
}