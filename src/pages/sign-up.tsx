import { Typography, TextField } from "@mui/material";
import { InputButtonWithBackground, FormContainer, Wrapper } from "../styles";

export const SignUpPage = () => {
  return (
    <Wrapper>
      <Typography variant="h2" align="center" sx={{ color: "#111111" }}>
        Create an account
      </Typography>
      <Typography variant="body1" align="center" sx={{ color: "#595858" }}>
        Already have an account? Log in
      </Typography>

      <FormContainer action="">
        <Typography component="label" htmlFor="email" sx={{ color: "#595858" }}>What's your email?</Typography>
        <TextField
          fullWidth
          type="email"
          id="email"
          name="email"
          variant="outlined"
          placeholder="Enter your email address"
          sx={{ mt: 1.25, mb: 5 }}
        />

        <Typography component="label" htmlFor="password" sx={{ mb: 1.25, color: "#595858" }}>Create a password</Typography>
        <TextField
          fullWidth
          type="password"
          name="password"
          id="password"
          variant="outlined"
          placeholder="Create a password"
          helperText="Use 8 or more characters with a mix of letters, numbers & symbols"
          sx={{ mt: 1.25, mb: 5 }}
        />

        <InputButtonWithBackground fullWidth type="submit" variant="contained">Create an account</InputButtonWithBackground>
      </FormContainer>
    </Wrapper>
  );
};
