import { Box, Typography } from "@mui/material"
import { Articles } from "../components/shared/Articles"

export const UserArticle = () => {
  return (

    <Box component="main" sx={{ maxWidth: 1440, mt: 7, mx: "auto" }}>
      <Box component="section">
        <Typography variant="h2" sx={{ mx: 1.5, py: 1.5, borderBottom: "1px solid #EAEAEA" }}>my articles</Typography>

        <Box sx={{ maxWidth: 786, mt: 9, mx: "auto", mb: 2 }}>
          <Articles />
        </Box>
      </Box>
    </Box>
  )
}