import { Box, Grid, InputAdornment, TextField, Typography } from "@mui/material";
import VisibilityIcon from '@mui/icons-material/Visibility';
import { FormContainer, InputButtonWithBackground, UploadedImage, Wrapper } from "../styles";
import { FileUploadInput } from "../components/shared/FileUploadInput";

export const EditProfilePage = () => {
  return (
    <Wrapper>
      <Typography variant="h2" align="center" sx={{ color: "#111111" }}>
        edit profile
      </Typography>

      <Typography variant="body1" align="center" sx={{ color: "#595858" }}>
        Personalize/Edit your profile
      </Typography>

      <FormContainer action="">
        <Box sx={{ display: "flex", alignItems: "flex-end", mb: 5 }}>
          <UploadedImage />
          <FileUploadInput />
        </Box>

        <Grid container spacing={5} sx={{ mb: 5 }}>
          <Grid item sm={6} xs={12}>
            <Typography component="label" htmlFor="first-name" sx={{ color: "#595858" }}>First name</Typography>

            <TextField
              fullWidth
              type="text"
              name="first-name"
              id="first-name"
              variant="outlined"
              placeholder="First name"
              sx={{ mt: 1.25 }}
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <Typography component="label" htmlFor="last-name" sx={{ color: "#595858" }}>Last name</Typography>

            <TextField
              fullWidth
              type="text"
              name="last-name"
              id="last-name"
              variant="outlined"
              placeholder="Last name"
              sx={{ mt: 1.25 }}
            />
          </Grid>
        </Grid>

        <Typography component="label" htmlFor="email" sx={{ color: "#595858" }}>What's your email?</Typography>
        <TextField
          fullWidth
          type="email"
          id="email"
          name="email"
          variant="outlined"
          value="fahad.mirza@kwanso.com"
          InputProps={{
            sx: {
              "&:focus-within fieldset, &:focus-visible fieldset": {
                borderColor: "#D8D8D8 !important",
              }
            },
            readOnly: true
          }}
          sx={{ backgroundColor: "#D8D8D8", borderRadius: "20px", mt: 1.25, mb: 5 }}
        />

        <Typography component="label" htmlFor="password" sx={{ mb: 1.25, color: "#595858" }}>Type new password</Typography>
        <TextField
          fullWidth
          type="password"
          name="password"
          id="password"
          variant="outlined"
          placeholder="Password"
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <VisibilityIcon />
              </InputAdornment>
            )
          }}
          helperText="Use 8 or more characters with a mix of letters, numbers & symbols"
          sx={{ mt: 1.25, mb: 5 }}
        />

        <Typography component="label" htmlFor="password-again" sx={{ mb: 1.25, color: "#595858" }}>Type new password again</Typography>
        <TextField
          fullWidth
          type="password"
          name="password-again"
          id="password-again"
          variant="outlined"
          placeholder="Password"
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <VisibilityIcon />
              </InputAdornment>
            )
          }}
          helperText="Use 8 or more characters with a mix of letters, numbers & symbols"
          sx={{ mt: 1.25, mb: 5 }}
        />

        <InputButtonWithBackground fullWidth type="submit" variant="contained">save changes</InputButtonWithBackground>
      </FormContainer>
    </Wrapper>
  )
}