import { Box, Pagination, Typography } from "@mui/material"
import { searchResultStyle } from "../styles/styleConstants"
import { Articles } from "../components/shared/Articles"

export const SearchResult = () => {
  return (
    <Box component="main" sx={{ maxWidth: 1440, mt: 7, mx: "auto" }}>
      <Box component="section">
        <Typography variant="h6" sx={searchResultStyle}>Search results for <Box component="span" color="#222">Travel</Box></Typography>

        <Box sx={{ maxWidth: 786, mt: 9, mx: "auto", mb: 2 }}>
          <Articles />

          <Pagination count={3} variant="outlined" shape="rounded" sx={{ mt: 9 }} />
        </Box>
      </Box>
    </Box>
  )
}