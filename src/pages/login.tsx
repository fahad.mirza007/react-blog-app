import { Typography, TextField, InputAdornment, FormGroup, FormControlLabel, Checkbox, Divider } from "@mui/material";
import VisibilityIcon from '@mui/icons-material/Visibility';
import { Wrapper, FormContainer, ForgotPasswordLink, InputButtonWithBackground, InputButtonWithoutBackground } from "../styles";

export const LoginPage = () => {
  return (
    <Wrapper>
      <Typography variant="h2" align="center" sx={{ color: "#111111" }}>
        Log in
      </Typography>

      <FormContainer action="">
        <Typography component="label" htmlFor="email" sx={{ color: "#595858" }}>Email address or user name</Typography>
        <TextField
          fullWidth
          type="email"
          id="email"
          name="email"
          variant="outlined"
          placeholder="Email address or user name"
          sx={{ mt: 1.25, mb: 5 }}
        />

        <Typography component="label" htmlFor="password" sx={{ mb: 1.25, color: "#595858" }}>Create a password</Typography>
        <TextField
          fullWidth
          type="password"
          name="password"
          id="password"
          variant="outlined"
          placeholder="Password"
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <VisibilityIcon />
              </InputAdornment>
            )
          }}
          helperText={<ForgotPasswordLink to="#">Forgot your password</ForgotPasswordLink>}
          sx={{ mt: 1.25, mb: 5 }}
        />

        <FormGroup sx={{ mb: 5 }}>
          <FormControlLabel label="Remember Me" control={<Checkbox />} />
        </FormGroup>

        <InputButtonWithBackground fullWidth type="submit" variant="contained">Log in</InputButtonWithBackground>
      </FormContainer>

      <Divider sx={{ my: 5, border: "1px solid #EAEAEA" }} />

      <Typography variant="h6" align="center" sx={{ textTransform: "capitalize", color: "#595858", mb: 2 }}>Don't have an account?</Typography>

      <InputButtonWithoutBackground fullWidth type="button" variant="outlined">sign up</InputButtonWithoutBackground>
    </Wrapper>
  )
}