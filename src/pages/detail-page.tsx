import { Box, Grid, Typography } from "@mui/material"
import { BadgeText } from "../components/shared/BadgeText"
import { ImageUserWrapper } from "../components/shared/ImageUserWrapper"
import { DetailPageBanner } from "../styles"
import storyImage from '../images/top-stories.jpeg';
import { storyImageStyle } from "../styles/styleConstants"

const articles = [
  {
    id: 1,
    image: storyImage,
    title: "The Impact of Technology on the Workplace: How Technology is Changing",
    description: "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident."
  },
  {
    id: 2,
    image: storyImage,
    title: "The Impact of Technology on the Workplace: How Technology is Changing",
    description: "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident."
  },
  {
    id: 3,
    image: storyImage,
    title: "The Impact of Technology on the Workplace: How Technology is Changing",
    description: "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident."
  }
]

export const DetailPage = () => {
  return (
    <Box component="main" sx={{ maxWidth: 1440, mt: 7, mx: "auto" }}>
      <Box component="section">
        <BadgeText />
        <Typography variant="h2" sx={{ maxWidth: 782, mt: 2, mb: 3 }}>
          The Impact of Technology on the Workplace: How Technology is Changing
        </Typography>
        <ImageUserWrapper />
      </Box>

      <Box component="section" sx={{ mt: 3 }}>
        <DetailPageBanner />
        <Typography variant="body1" color="#666" sx={{ maxWidth: 856, m: "40px auto", textAlign: "justify" }}>
          Did you come here for something in particular or just general Riker-bashing? And blowing into maximum warp speed, you appeared for an instant to be in two places at once. We have a saboteur aboard.
          We know you’re dealing in stolen ore. But I wanna talk about the assassination attempt on Lieutenant Worf.
          Could someone survive inside a transporter buffer for 75 years? Fate. It protects fools, little children, and ships.
        </Typography>
      </Box>

      <Box component="section" sx={{ mb: 2 }}>
        <Typography variant="h2" sx={{ mx: 1.5, py: 1.5 }}>what to read next</Typography>

        <Grid container spacing={4}>
          {articles.map((article) => (
            <Grid key={article.id} item sm={4} xs={12}>
              <Box component="img" src={article.image} alt={article.image} sx={storyImageStyle} />
              <ImageUserWrapper />
              <Typography variant="h4" sx={{ my: 2.5 }}>{article.title}</Typography>
              <Typography variant="body1" color="#232536" sx={{ opacity: .7 }}>{article.description}</Typography>
            </Grid>
          ))}
        </Grid>
      </Box>
    </Box>
  )
}