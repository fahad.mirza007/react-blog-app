import { Box } from "@mui/material";
import { TopStories } from "../components/TopStories";
import { TrendingStories } from "../components/TrendingStories";
import { TopDestinations } from "../components/TopDestinations";

export const HomePage = () => {
  return (
    <Box component="main" sx={{ maxWidth: 1440, mt: 7, mx: "auto" }}>
      <Box component="section" sx={{ mb: 8 }}>
        <TopStories />
      </Box>

      <Box component="section" sx={{ mb: 8 }}>
        <TrendingStories />
      </Box>

      <Box component="section" sx={{ mb: 8 }}>
        <TopDestinations />
      </Box>
    </Box>
  );
}