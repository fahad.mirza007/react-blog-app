import { Box, MenuItem, TextField, Typography } from "@mui/material"
import { CreateArticleFormContainer, InputButtonWithBackground } from "../styles"
import { FileUploadInput } from "../components/shared/FileUploadInput"

const categories = [
  {
    id: 1,
    name: "Category 1"
  },
  {
    id: 2,
    name: "Category 2"
  },
  {
    id: 3,
    name: "Category 3"
  }
];

export const CreateArticle = () => {
  return (
    <Box component="main" sx={{ maxWidth: 1440, mt: 7, mx: "auto" }}>
      <Box component="section">
        <Typography variant="h2" sx={{ mx: 1.5, py: 1.5, borderBottom: "1px solid #EAEAEA" }} >
          create article
        </Typography>

        <CreateArticleFormContainer action="">
          <Typography component="label" htmlFor="title" sx={{ color: "#595858" }}>Give it a title</Typography>
          <TextField
            fullWidth
            type="text"
            id="title"
            name="title"
            variant="outlined"
            sx={{ mt: 1.25, mb: 5 }}
          />

          <Typography component="label" htmlFor="category" sx={{ color: "#595858" }}>Category</Typography>
          <TextField
            fullWidth
            select
            type="text"
            id="category"
            name="category"
            variant="outlined"
            defaultValue="Category 1"
            SelectProps={{ MenuProps: { disableScrollLock: true } }}
            sx={{ mt: 1.25, mb: 5 }}
          >
            {categories.map((category) => (
              <MenuItem key={category.id} value={category.name}>{category.name}</MenuItem>
            ))}
          </TextField>

          <Typography component="label" htmlFor="comments" sx={{ color: "#595858" }}>Write something about it</Typography>
          <TextField
            fullWidth
            type="text"
            id="comments"
            name="comments"
            variant="outlined"
            multiline
            rows={8}
            sx={{ mt: 1.25, mb: 5 }}
          />

          <Box sx={{ mb: 5 }}>
            <FileUploadInput />
          </Box>

          <InputButtonWithBackground fullWidth type="submit" variant="contained">publish article</InputButtonWithBackground>
        </CreateArticleFormContainer>
      </Box>
    </Box>
  )
}