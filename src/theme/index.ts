import { createTheme } from "@mui/material";

export const customUITheme = createTheme({
  palette: {
    primary: {
      main: "#111111",
    },
  },

  typography: {
    fontFamily: ["Poppins", "sans-serif"].join(","),

    h1: {
      fontSize: 48,
      fontWeight: 700,
      lineHeight: "48px",
    },

    h2: {
      fontSize: 36,
      fontWeight: 600,
      lineHeight: "54px",
      textTransform: "capitalize",
    },

    h3: {
      fontSize: 32,
      fontWeight: 600,
      textTransform: "capitalize",
    },

    h4: {
      fontSize: 24,
      fontWeight: 600,
      textTransform: "capitalize",
    },

    h6: {
      lineHeight: "27px",
    },

    body1: {
      fontSize: 15,
      fontWeight: 400,
      lineHeight: "22.5px",
    },
  },

  components: {
    MuiTypography: {
      styleOverrides: {
        root: {
          "& a": {
            color: "#FFFFFF",
            textDecoration: "none",
          },
        },
      },
    },

    MuiTextField: {
      styleOverrides: {
        root: {
          "&:hover fieldset": {
            borderColor: "#D8D8D8 !important",
          },

          "&:focus-within fieldset, &:focus-visible fieldset": {
            borderColor: "#111111 !important",
          },
        },
      },
    },

    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          padding: "16px 24px 15px",
        },

        notchedOutline: {
          border: "1px solid #D8D8D8",
          outline: "none",
          borderRadius: 20,
        },

        input: {
          padding: 0,
          color: "#595858",
        },
      },
    },

    MuiFormHelperText: {
      styleOverrides: {
        root: {
          color: "#595858",
          marginTop: 4,
          marginLeft: "unset",
          fontSize: 14,
        },
      },
    },

    MuiButton: {
      styleOverrides: {
        root: {
          fontSize: 24,
          lineHeight: "36px",
          fontWeight: 600,
          textTransform: "capitalize",
          padding: 13,
          borderRadius: 40,
        },
      },
    },

    MuiSvgIcon: {
      styleOverrides: {
        root: {
          width: 24,
          height: 24,
          borderRadius: 4,
          outline: "none",
        },
      },
    },

    MuiChip: {
      styleOverrides: {
        root: {
          maxWidth: "fit-content",
          fontWeight: 400,
          lineHeight: "22.5px",
          backgroundColor: "#4B6BFB",
          color: "#FFFFFF",
          textTransform: "capitalize",
          borderRadius: 6,
        },
      },
    },

    MuiPagination: {
      styleOverrides: {
        root: {
          "& ul": {
            justifyContent: "center",
          },
        },
      },
    },

    MuiPaginationItem: {
      styleOverrides: {
        root: {
          backgroundColor: "transparent !important",
          fontSize: 15,
          lineHeight: 15,
          borderRadius: 8,
          minWidth: 41,
          height: 41,
          borderColor: "#111",
        },
      },
    },
  },
});
