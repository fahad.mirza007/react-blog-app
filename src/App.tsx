import { CssBaseline, ThemeProvider } from '@mui/material';
import { customUITheme } from './theme';
import { createBrowserRouter, RouterProvider, Route, Routes } from 'react-router-dom';
import { HomePage } from './pages';
import { LoginPage } from './pages/login';
import { SignUpPage } from './pages/sign-up';
import { EditProfilePage } from './pages/edit-profile';
import { DetailPage } from './pages/detail-page';
import { SearchResult } from './pages/search-result';
import { UserArticle } from './pages/user-article';
import { CreateArticle } from './pages/create-article';
import { Navbar } from './components/shared/Navbar';

export const App = () => <RouterProvider router={router} />

const Root = () => {
  return (
    <ThemeProvider theme={customUITheme}>
      <CssBaseline />
      
      <Navbar />
      
      <Routes>
        <Route path='/' element={<HomePage />} />
        <Route path='/login' element={<LoginPage />} />
        <Route path='/sign-up' element={<SignUpPage />} />
        <Route path='/edit-profile' element={<EditProfilePage />} />
        <Route path='/detail-page' element={<DetailPage />} />
        <Route path='/search' element={<SearchResult />} />
        <Route path='/article' element={<UserArticle />} />
        <Route path='/create-article' element={<CreateArticle />} />
      </Routes>
    </ThemeProvider>
  )
}

const router = createBrowserRouter([{ path: "*", Component: Root }]);
